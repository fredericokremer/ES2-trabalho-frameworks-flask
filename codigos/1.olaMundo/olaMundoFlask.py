#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
aplicacao = Flask(__name__)

@aplicacao.route('/')
def ola_mundo():
    return 'Olá, mundo!'

if __name__ == "__main__":
    aplicacao.run(debug=True)
