#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, send_from_directory, render_template
import os
aplicacao = Flask(__name__)

@aplicacao.route('/files/<path:path>')
def file_server(path):
    return send_from_directory("public", filename=path)

@aplicacao.route('/')
def render_page():
    isfile = lambda name: os.path.isfile("public/" + name)
    files_names = [file_name for file_name in os.listdir("public") if isfile(file_name)]
    files_sizes = [os.path.getsize("public/"+file_name) for file_name in files_names]
    files = zip(files_names, files_sizes)
    return render_template("files.html", files=files);

if __name__ == "__main__":
    aplicacao.run(debug=True)
