# Trabalho de frameworks para desenvolvimento web: Python Flask

Esta repositório serve para armazenar a documentação e o código-fonte
do projeto sobre *frameworks* para desenvolvimento *web* da disciplina
de Engenharia de Software II.

**Aluno:** Frederico Schmitt Kremer

## O que é o Flask?

O **Flask** é um *framework* para desenvolvimento *web* em linguagem de 
programação Python, sendo uma alternativa aos *frameworks* **Django** e 
**Pyramid**. Seu grande diferencial é o fato de ser mais leve e conciso
em relação aos demais que os demais, o que facilita o processo de 
aprendizagem e desenvolvimento de ferramentas de menor complexidade,
apesar de resultar em uma disponibilidade de recursos para o desenvolvedor. 
Entretanto, muitas *features* encontradas em outros *frameworks* podem
ser integradas ao **Flask** com uso de bibliotecas auxiliares,
como a **SQLAlchemy** para mapeamento objeto-relacional e a **Requests** para
comunicação com outros *webservices*.

## Instalando

Como muitos outros pacotes da linguagem Python, o Flask pode ser instalado
de diferentes formas, sendo as principais:

- Através do código-fonte
- Através do gerenciador de pacotes nativo da linguagem Python
- Através de gerenciadores de pacote externos (ex: `apt`)

### Instalando 

### Instalando com o pip / pip3

#### Instalar o pacote para todos usuários para Python 2.x

`sudo pip install flask`

#### Instalar o pacote para apenas o usuário atual para Python 2.x

`pip install flask --user`

#### Instalar o pacote para todos usuários para Python 3.x

`sudo pip3 install flask`

#### Instalar o pacote para apenas o usuário atual no Python 3.x

`pip3 install flask --user`

#### Instalar

### Instalando com apt (Ubuntu Linux)

## "Olá, mundo!" com Flask

Para testar se o **Flask** está devidamente instalado, faça o seguinte teste:

- Crie uma *script* em linguagem Python com o código abaixo.

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
aplicacao = Flask(__name__)

@aplicacao.route('/')
def ola_mundo():
    return 'Olá, mundo!'
```
- Salve o *script* como `olaMundoFlask.py`.
- Defina as seguintes variáveis de ambiente no seu *shell*: `FLASK_APP` e `FLASK_ENV`. `FLASK_APP` deve armazenar o caminho
para o *script* principal da sua aplicação. Já a variável `FLASK_ENV` guarda o nome do seu ambiente de execução (ex: "development").

```bash
export FLASK_APP=olaMundoFlask.py
export FLASK_ENV=development
```
  
- Execute o *script* com o comando: `flask run`.
- Acesse o endereço `http://localhost:5000` 

## Criando um CRUD com Python Flask

Um tutorial passo-a-passo de como criar um sistema CRUD (***C**reate, **R**etrieve, **U**pdate and **D**elete*) 
utilizando Flask está disponível na Wiki deste repositório e neste vídeo (em breve).  

