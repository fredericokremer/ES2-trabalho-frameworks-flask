#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, send_from_directory
aplicacao = Flask(__name__)

@aplicacao.route('/<path:path>')
def paginas_estaticas(path):
    return send_from_directory('public/', path)

if __name__ == "__main__":
    aplicacao.run(debug=True)
