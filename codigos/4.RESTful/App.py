#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, render_template, redirect
from flask_sqlalchemy import SQLAlchemy

App = Flask(__name__)
App.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"
Database = SQLAlchemy(App)

class Todo(Database.Model):

    id = Database.Column(Database.Integer, primary_key=True)
    title = Database.Column(Database.Text)
    description = Database.Column(Database.Text)

    def __init__(self, titulo, descricao):
        self.title = titulo
        self.description = descricao

Database.create_all()

@App.route('/')
def index():
    return render_template("index.html", todo=None, todos=Todo.query.all())

@App.route('/todos/<int:todoId>', methods=['GET'])
def list_todos(todoId):
    todo = Todo.query.filter_by(id=todoId).first()
    return render_template("index.html", todo=todo, todos=Todo.query.all())

@App.route('/todos', methods=['POST'])
def create_todos():

    dados_form = request.form
    novo_todo = Todo(dados_form["titulo"], dados_form["descricao"])
    Database.session.add(novo_todo)
    Database.session.commit()

    return redirect("/")

@App.route('/todos/<int:todoId>/update', methods=['POST'])
def update_todos(todoId):

    todo_atualizado = {"title"    : request.form["titulo"], 
                       "description" : request.form["descricao"]}
    Todo.query.filter_by(id=todoId).update(todo_atualizado)
    Database.session.commit()

    return redirect("/")

@App.route('/todos/<int:todoId>/delete', methods=['POST'])
def delete_todos(todoId):

    Todo.query.filter_by(id=todoId).delete()

    Database.session.commit()
    return redirect("/")

if __name__ == "__main__":

    App.run(debug=True)
